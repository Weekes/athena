# Athena #

Athena, also referred to as Athene is the goddess of wisdom, courage, inspiration, civilization, law and justice, strategic warfare, mathematics, strength, strategy, the arts, crafts, and skill.

### What is this repository for? ###

This Nuxt & Vue Project is a proof off concept prototype. It's ugly and doesn't at all demonstrate what could be fully realised using Vue as our primary Frontend Framework, but it does give some insight
into the mechanisms of Vue, it's formatting and controls.

This project utilise Axios to fetch live data from a free public JSON API. It has Authentication built in with Bearer Tokens, but disabled due to no native PHP (Laravel) Framework running it, albeit it's 
easy enough to configure if this prototype was to be taken further.

It's making use of the NuxtRouter & Page Structure tools to showcase how quick & easy it is to build large scale applications within Vue.

### How do I get set up? ###

* Clone the repo and then `cd athena`
* Install the APP with `yarn`
* Run the APP with `yarn dev` which will then boot up, and provide you a with a link on your localhost
* When greeted with the login form, just press "Sign In". This may take a second as it should have an Authentication Layer between, but this is bypassed for this protoype.
* Navigate around, look at the code, rip it apart, try to change things and break things. Have at it!

### Tools ###

I _highly_ recommend installing VueDevTools on your Browser if it is supported. This will allow you to easy review some of the Components, VuexStore and other elements in your DevTools.
