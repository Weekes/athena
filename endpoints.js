export default {
    user: 'users',
    post: 'posts',
    comment: 'comments',
    album: 'albums',
    photo: 'photos',
    todo: 'todos'
};
